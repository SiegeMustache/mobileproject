﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoleController : MonoBehaviour
{
    public GameObject snapLocation;
    public float speedRotation;
    public bool isClockwise;
    public bool overrideBehaviourRotation;
    public bool canMove;
    public GameObject wayPoint1;
    public GameObject wayPoint2;
    public GameObject startingWaypoint;
    public float moveSpeed;

    private GameObject waypointTarget;
    private GameObject target;
    private enum PoleState { Idle, Rotate};
    private PoleState currentState;

    // Start is called before the first frame update
    void Start()
    {
        target = PlayerController.instance.gameObject;

        if (wayPoint1 == null && wayPoint2 == null)
        {
            canMove = false;
        }
        else if (wayPoint1 == null && wayPoint2 != null)
        {
            canMove = false;
            transform.position = wayPoint2.transform.position;
        }
        else if(wayPoint1 != null && wayPoint2 == null)
        {
            canMove = false;
            transform.position = wayPoint1.transform.position;
        }
        else if(wayPoint1 != null && wayPoint2 != null)
        {
            canMove = true;
            if(startingWaypoint == null)
            {
                transform.position = wayPoint1.transform.position;
                waypointTarget = wayPoint2;
            }
            else if(startingWaypoint != null)
            {
                transform.position = startingWaypoint.transform.position;
                waypointTarget = wayPoint1;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(canMove == false)
        {
            switch (currentState)
            {
                case PoleState.Idle:
                    {
                        Vector3 dir = target.transform.position - transform.position;
                        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                        break;
                    }
                case PoleState.Rotate:
                    {
                        if (isClockwise == true)
                        {
                            transform.Rotate(Vector3.forward * Time.deltaTime * speedRotation);
                        }
                        else if (isClockwise == false)
                        {
                            transform.Rotate(-Vector3.forward * Time.deltaTime * speedRotation);
                        }
                        break;
                    }
            }
        }
        else if(canMove == true)
        {
            if (waypointTarget == wayPoint1)
            {
                float step = moveSpeed * Time.deltaTime;
                transform.position = Vector2.MoveTowards(transform.position, wayPoint1.transform.position, step);
                if (Vector2.Distance(transform.position, wayPoint1.transform.position) <= 0.2f)
                {
                    waypointTarget = wayPoint2;
                }

                if (this.gameObject != PlayerController.instance.snapTarget)
                {
                    Vector3 dir = target.transform.position - transform.position;
                    float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                    transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                }
                else if (this.gameObject == PlayerController.instance.snapTarget)
                {
                    if (isClockwise == true)
                    {
                        transform.Rotate(Vector3.forward * Time.deltaTime * speedRotation);
                    }
                    else if (isClockwise == false)
                    {
                        transform.Rotate(-Vector3.forward * Time.deltaTime * speedRotation);
                    }
                }
            }
            else if (waypointTarget == wayPoint2)
            {
                float step = moveSpeed * Time.deltaTime;
                transform.position = Vector2.MoveTowards(transform.position, wayPoint2.transform.position, step);
                if (Vector2.Distance(transform.position, wayPoint2.transform.position) <= 0.2f)
                {
                    waypointTarget = wayPoint1;
                }

                if (this.gameObject != PlayerController.instance.snapTarget)
                {
                    Vector3 dir = target.transform.position - transform.position;
                    float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                    transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                }
                else if(this.gameObject == PlayerController.instance.snapTarget)
                {
                    if (isClockwise == true)
                    {
                        transform.Rotate(Vector3.forward * Time.deltaTime * speedRotation);
                    }
                    else if (isClockwise == false)
                    {
                        transform.Rotate(-Vector3.forward * Time.deltaTime * speedRotation);
                    }
                }
            }
        }

        if(this.gameObject != PlayerController.instance.snapTarget)
        {
            currentState = PoleState.Idle;
        }
        else if(this.gameObject == PlayerController.instance.snapTarget)
        {
            currentState = PoleState.Rotate;
        }
    }
}
