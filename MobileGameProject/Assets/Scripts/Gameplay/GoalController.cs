﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalController : MonoBehaviour
{
    public bool isEnemyGoal;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if(isEnemyGoal == true)
            {
                GameManager.instance.PlayerWins();
                AudioManager.instance.PlayClip(AudioManager.instance.audioClips[1], 0.5f, true);
                PlayerController.instance.SlowDown(5);
                PlayerController.instance.matchEnded = true;
                TimerManager.instance.timeStopped = true;
            }
            else if(isEnemyGoal == false)
            {
                GameManager.instance.PlayerLost();
                AudioManager.instance.PlayClip(AudioManager.instance.audioClips[0], 0.7f);
                PlayerController.instance.SlowDown(5);
                PlayerController.instance.matchEnded = true;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            PlayerController.instance.SlowDown(5);
        }
    }
}
