﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerManager : Singleton<TimerManager>
{
    [HideInInspector]
    public float timer;
    [HideInInspector]
    public float minutes;
    [HideInInspector]
    public float seconds;
    [HideInInspector]
    public bool timeStopped;


    private void Awake()
    {
        GameManager.instance.ResetLevel += ResetTimer;
        GameManager.instance.ExtraResetLevel += ResetTimer;
        GameManager.instance.ResumeLevel += ActivateTimer;
        GameManager.instance.PauseLevel += StopTimer;
        GameManager.instance.PlayerWin += StopTimer;
        GameManager.instance.PlayerLose += StopTimer;
    }

    private void OnDestroy()
    {
        GameManager.instance.ResetLevel -= ResetTimer;
        GameManager.instance.ExtraResetLevel -= ResetTimer;
        GameManager.instance.ResumeLevel -= ActivateTimer;
        GameManager.instance.PauseLevel -= StopTimer;
        GameManager.instance.PlayerWin -= StopTimer;
        GameManager.instance.PlayerLose -= StopTimer;
    }

    private void Start()
    {
        timer = 0;
        minutes = 0;
        seconds = 0;
        timeStopped = false;
    }

    private void Update()
    {
        if(timeStopped == false)
        {
            timer += Time.deltaTime;
        }


        seconds = Mathf.FloorToInt(timer % 60);
        minutes = Mathf.Floor(timer / 60);
    }

    public void ResetTimer()
    {
        timer = 0;
        minutes = 0;
        seconds = 0;
        timeStopped = false;
    }

    public void ActivateTimer()
    {
        timeStopped = false;
    }

    public void StopTimer()
    {
        timeStopped = true;
    }

    public void SaveTimer(float timer)
    {
        if(SaveManager.instance.currentLevel == 1)
        {
            if(SaveManager.instance.playerData.level1Time != 0)
            {
                if(SaveManager.instance.playerData.level1Time > timer)
                {
                    SaveManager.instance.playerData.level1Time = timer;
                    SaveManager.instance.SaveData();
                }
            }
            else if(SaveManager.instance.playerData.level1Time == 0)
            {
                SaveManager.instance.playerData.level1Time = timer;
                SaveManager.instance.playerData.level2Locked = 0;
                SaveManager.instance.SaveData();
            }
        }
        else if (SaveManager.instance.currentLevel == 2)
        {
            if (SaveManager.instance.playerData.level2Time != 0)
            {
                if (SaveManager.instance.playerData.level2Time > timer)
                {
                    SaveManager.instance.playerData.level2Time = timer;
                    SaveManager.instance.SaveData();
                }
            }
            else if (SaveManager.instance.playerData.level2Time == 0)
            {
                SaveManager.instance.playerData.level2Time = timer;
                SaveManager.instance.playerData.level3Locked = 0;
                SaveManager.instance.SaveData();
            }
        }
        else if (SaveManager.instance.currentLevel == 3)
        {
            if (SaveManager.instance.playerData.level3Time != 0)
            {
                if (SaveManager.instance.playerData.level3Time > timer)
                {
                    SaveManager.instance.playerData.level3Time = timer;
                    SaveManager.instance.SaveData();
                }
            }
            else if (SaveManager.instance.playerData.level3Time == 0)
            {
                SaveManager.instance.playerData.level3Time = timer;
                SaveManager.instance.playerData.level4Locked = 0;
                SaveManager.instance.SaveData();
            }
        }
        else if (SaveManager.instance.currentLevel == 4)
        {
            if (SaveManager.instance.playerData.level4Time != 0)
            {
                if (SaveManager.instance.playerData.level4Time > timer)
                {
                    SaveManager.instance.playerData.level4Time = timer;
                    SaveManager.instance.SaveData();
                }
            }
            else if (SaveManager.instance.playerData.level4Time == 0)
            {
                SaveManager.instance.playerData.level4Time = timer;
                SaveManager.instance.playerData.level5Locked = 0;
                SaveManager.instance.SaveData();
            }
        }
        else if (SaveManager.instance.currentLevel == 5)
        {
            if (SaveManager.instance.playerData.level5Time != 0)
            {
                if (SaveManager.instance.playerData.level5Time > timer)
                {
                    SaveManager.instance.playerData.level5Time = timer;
                    SaveManager.instance.SaveData();
                }
            }
            else if (SaveManager.instance.playerData.level5Time == 0)
            {
                SaveManager.instance.playerData.level5Time = timer;
                SaveManager.instance.SaveData();
            }
        }
    }
}
