﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public GameObject screenCentre;
    private void LateUpdate()
    {
        float interpolation = 0.8f * Time.deltaTime;

        Vector3 position = this.transform.position;
        position.y = Mathf.Lerp(this.transform.position.y, PlayerController.instance.transform.position.y, interpolation);
        position.x = Mathf.Lerp(this.transform.position.x, screenCentre.transform.position.x, interpolation);

        this.transform.position = position;
    }
}
