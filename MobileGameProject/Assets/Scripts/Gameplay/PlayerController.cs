﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : Singleton<PlayerController>
{

    public Action PlayerDeath;

    public float snapRange;
    public float playerSpeed;
    public float throwForce;
    public float slowFactor;
    public float speedBoostFactor;
    public Rigidbody2D rb;
    public LayerMask poleLayer;
    public int lives;
    public GameObject initialPole;

    [HideInInspector]
    public GameObject snapTarget;
    [HideInInspector]
    public bool snappingEnabled;
    [HideInInspector]
    public int currentLives;
    [HideInInspector]
    public bool matchEnded;

    private GameObject lastPlayer;
    private enum PlayerState { Free, Snapped };
    private PlayerState currentState;



    private void Awake()
    {
        GameManager.instance.ResetLevel += OnReset;
        GameManager.instance.ExtraResetLevel += OnExtraReset;
    }

    private void OnDestroy()
    {
        GameManager.instance.ResetLevel -= OnReset;
        GameManager.instance.ExtraResetLevel -= OnExtraReset;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentState = PlayerState.Snapped;
        snappingEnabled = true;
        snapTarget = initialPole;
        currentLives = 3;
        lastPlayer = null;
        matchEnded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && lives != 0)
        {
            ToggleSnap();
        }
        
        if(rb.velocity.x == 0 && rb.velocity.y == 0 && currentState == PlayerState.Free)
        {
            Death();
        }

        switch (currentState)
        {
            case PlayerState.Free:
                {
                    snapTarget = null;

                    Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, snapRange, poleLayer);

                    if (colliders != null)
                    {
                        foreach (Collider2D collider in colliders)
                        {
                            if (snapTarget == null)
                            {
                                snapTarget = collider.gameObject;
                            }
                            if (snapTarget != null)
                            {
                                if (Vector2.Distance(transform.position, snapTarget.transform.position) > Vector2.Distance(transform.position, collider.gameObject.transform.position))
                                {
                                    snapTarget = collider.gameObject;
                                }
                            }
                        }
                    }
                    if(snappingEnabled == true)
                    {
                        if (snapTarget != null)
                        {
                            if (snapTarget.GetComponent<PoleController>().overrideBehaviourRotation == false)
                            {
                                if (transform.position.x <= snapTarget.transform.position.x)
                                {
                                    snapTarget.GetComponent<PoleController>().isClockwise = false;
                                }
                                else if (transform.position.x > snapTarget.transform.position.x)
                                {
                                    snapTarget.GetComponent<PoleController>().isClockwise = true;
                                }
                            }
                            lastPlayer = snapTarget;
                            currentState = PlayerState.Snapped;
                        }
                        else if(snapTarget == null)
                        {
                            snappingEnabled = false;
                        }
                    }

                    if(rb.velocity.magnitude < 2 && rb.velocity.magnitude > -2 && TimerManager.instance.timeStopped == false)
                    {
                        Death();
                    }
                    break;
                }
            case PlayerState.Snapped:
                {
                    if (snappingEnabled == true)
                    {
                        transform.position = snapTarget.GetComponent<PoleController>().snapLocation.transform.position;
                        rb.freezeRotation = true;
                        rb.velocity = Vector2.zero;
                    }
                    else if (snappingEnabled == false)
                    {
                        rb.freezeRotation = false;
                        rb.AddForce(snapTarget.GetComponent<PoleController>().snapLocation.transform.right * throwForce, ForceMode2D.Impulse);
                        snapTarget = null;
                        AudioManager.instance.PlayClip(AudioManager.instance.audioClips[2], 0.7f);
                        currentState = PlayerState.Free;
                    }
                    break;
                }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Death();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "SlowObstacle")
        {
            if (rb.velocity.x < -1)
            {
                rb.velocity = new Vector2(rb.velocity.x + slowFactor, rb.velocity.y);
            }
            else if (rb.velocity.x > 1)
            {
                rb.velocity = new Vector2(rb.velocity.x - slowFactor, rb.velocity.y);
            }

            if (rb.velocity.y < -1)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + slowFactor);
            }
            else if (rb.velocity.y > 1)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y - slowFactor);
            }

            if (rb.velocity.x < 1 && rb.velocity.x > -1)
            {
                rb.velocity = new Vector2(0, rb.velocity.y);
            }
            if (rb.velocity.y < 1 && rb.velocity.y > -1)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
            }
        }
        else if(collision.tag == "SpeedBooster")
        {
            if (rb.velocity.x < 0)
            {
                rb.velocity = new Vector2(rb.velocity.x - speedBoostFactor, rb.velocity.y);
            }
            else if (rb.velocity.x >= 0)
            {
                rb.velocity = new Vector2(rb.velocity.x + speedBoostFactor, rb.velocity.y);
            }

            if (rb.velocity.y < 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y - speedBoostFactor);
            }
            else if (rb.velocity.y >= 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + speedBoostFactor);
            }
        }
    }

    public void ToggleSnap()
    {
        if(TimerManager.instance.timeStopped == false)
        {
            if (snappingEnabled == true)
            {
                snappingEnabled = false;
            }
            else if (snappingEnabled == false)
            {
                snappingEnabled = true;
            }
        }
    }

    public void Death()
    {
        PlayerDeath();
        currentState = PlayerState.Snapped;
        snappingEnabled = true;

        if(lastPlayer != null)
        {
            snapTarget = lastPlayer;
        }
        else if(lastPlayer == null)
        {
            snapTarget = initialPole;
        }

        currentLives--;
        if(currentLives <= 0)
        {
            GameManager.instance.PlayerLost();

        }
        else if(currentLives > 0)
        {
            AudioManager.instance.PlayClip(AudioManager.instance.audioClips[4], 0.6f);
        }
    }

    private void OnReset()
    {
        Start();
    }

    private void OnExtraReset()
    {
        currentState = PlayerState.Snapped;
        snappingEnabled = true;
        snapTarget = initialPole;
        currentLives = 5;
        lastPlayer = null;
        matchEnded = false;
    }

    public void SlowDown(float multiplier)
    {
        if (rb.velocity.x < -1)
        {
            rb.velocity = new Vector2(rb.velocity.x + slowFactor * multiplier, rb.velocity.y);
        }
        else if (rb.velocity.x > 1)
        {
            rb.velocity = new Vector2(rb.velocity.x - slowFactor * multiplier, rb.velocity.y);
        }

        if (rb.velocity.y < -1)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + slowFactor * multiplier);
        }
        else if (rb.velocity.y > 1)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y - slowFactor * multiplier);
        }

        if (rb.velocity.x < 1 && rb.velocity.x > -1)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        if (rb.velocity.y < 1 && rb.velocity.y > -1)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
        }
    }
}
