﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject wayPoint1;
    public GameObject wayPoint2;
    public float moveSpeed;
    public GameObject startingWayPoint;
    public bool canChase;
    public bool canMove;
    public float chaseRange;
    public float chaseSpeed;
    public float leashRange;
    private enum AIState { Patrol, Chase };
    private AIState currentState;

    private GameObject wayPointTarget;

    private void Awake()
    {
        GameManager.instance.ResetLevel += OnReset;
        PlayerController.instance.PlayerDeath += OnReset;
    }

    private void OnDestroy()
    {
        GameManager.instance.ResetLevel -= OnReset;
        PlayerController.instance.PlayerDeath -= OnReset;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (canMove == true)
        {
            if (startingWayPoint != null)
            {
                if (startingWayPoint == wayPoint1)
                {
                    transform.position = wayPoint1.transform.position;
                    wayPointTarget = wayPoint2;
                }
                else if (startingWayPoint == wayPoint2)
                {
                    transform.position = wayPoint2.transform.position;
                    wayPointTarget = wayPoint1;
                }
            }
            else if (startingWayPoint == null)
            {
                wayPointTarget = wayPoint1;
                transform.position = wayPoint2.transform.position;
            }
        }
        else if (canMove == false)
        {
            if (startingWayPoint != null)
            {
                transform.position = wayPoint1.transform.position;
            }
            else if (startingWayPoint == null)
            {
                //stand still
            }
        }

        currentState = AIState.Patrol;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = PlayerController.instance.gameObject.transform.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        if (canChase == true)
        {
            if (canMove == true)
            {
                switch (currentState)
                {
                    case AIState.Patrol:
                        {
                            if (wayPointTarget == wayPoint1)
                            {
                                float step = moveSpeed * Time.deltaTime;
                                transform.position = Vector2.MoveTowards(transform.position, wayPoint1.transform.position, step);
                                if (Vector2.Distance(transform.position, wayPoint1.transform.position) <= 0.2f)
                                {
                                    wayPointTarget = wayPoint2;
                                }
                            }
                            else if (wayPointTarget == wayPoint2)
                            {
                                float step = moveSpeed * Time.deltaTime;
                                transform.position = Vector2.MoveTowards(transform.position, wayPoint2.transform.position, step);
                                if (Vector2.Distance(transform.position, wayPoint2.transform.position) <= 0.2f)
                                {
                                    wayPointTarget = wayPoint1;
                                }
                            }

                            if (Vector2.Distance(transform.position, PlayerController.instance.transform.position) <= chaseRange && PlayerController.instance.matchEnded == false)
                            {
                                currentState = AIState.Chase;
                            }
                            break;
                        }
                    case AIState.Chase:
                        {
                            if (Vector2.Distance(transform.position, PlayerController.instance.transform.position) >= leashRange || PlayerController.instance.matchEnded == true)
                            {
                                currentState = AIState.Patrol;
                            }
                            else if (Vector2.Distance(transform.position, PlayerController.instance.transform.position) < leashRange && PlayerController.instance.matchEnded == false)
                            {
                                float step = chaseSpeed * Time.deltaTime;
                                transform.position = Vector2.MoveTowards(transform.position, PlayerController.instance.transform.position, step);
                            }
                            break;
                        }
                }
            }
            else if (canMove == false)
            {
                switch (currentState)
                {
                    case AIState.Patrol:
                        {
                            if (Vector2.Distance(transform.position, startingWayPoint.transform.position) > 0.1f)
                            {
                                float step = moveSpeed * Time.deltaTime;
                                transform.position = Vector2.MoveTowards(transform.position, startingWayPoint.transform.position, step);
                            }
                            else if (Vector2.Distance(transform.position, startingWayPoint.transform.position) <= 0.1f)
                            {
                                //stand still
                            }
                            if (Vector2.Distance(transform.position, PlayerController.instance.transform.position) <= chaseRange)
                            {
                                currentState = AIState.Chase;
                            }
                            break;
                        }
                    case AIState.Chase:
                        {
                            if (Vector2.Distance(transform.position, PlayerController.instance.transform.position) <= leashRange && PlayerController.instance.matchEnded == false)
                            {
                                float step = chaseSpeed * Time.deltaTime;
                                transform.position = Vector2.MoveTowards(transform.position, PlayerController.instance.transform.position, step);
                            }
                            else if (Vector2.Distance(transform.position, PlayerController.instance.transform.position) > leashRange || PlayerController.instance.matchEnded == true)
                            {
                                currentState = AIState.Patrol;
                            }
                            break;
                        }
                }
            }
        }
        else if (canChase == false)
        {
            if (canMove == true)
            {
                if (wayPointTarget == wayPoint1)
                {
                    float step = moveSpeed * Time.deltaTime;
                    transform.position = Vector2.MoveTowards(transform.position, wayPoint1.transform.position, step);
                    if (Vector2.Distance(transform.position, wayPoint1.transform.position) <= 0.2f)
                    {
                        wayPointTarget = wayPoint2;
                    }
                }
                else if (wayPointTarget == wayPoint2)
                {
                    float step = moveSpeed * Time.deltaTime;
                    transform.position = Vector2.MoveTowards(transform.position, wayPoint2.transform.position, step);
                    if (Vector2.Distance(transform.position, wayPoint2.transform.position) <= 0.2f)
                    {
                        wayPointTarget = wayPoint1;
                    }
                }
            }
            else if (canMove == false)
            {
                transform.position = startingWayPoint.transform.position;
            }
        }
    }

    public void OnReset()
    {
        Start();
    }
}

