﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadLevel1()
    {
        SceneManager.LoadScene("Level1", LoadSceneMode.Single);
    }

    public void LoadLevel2()
    {
        if(SaveManager.instance.playerData.level2Locked == 0)
        {
            SceneManager.LoadScene("Level2", LoadSceneMode.Single);
        }
    }

    public void LoadLevel3()
    {
        if(SaveManager.instance.playerData.level3Locked == 0)
        {
            SceneManager.LoadScene("Level3", LoadSceneMode.Single);
        }
    }

    public void LoadLevel4()
    {
        if(SaveManager.instance.playerData.level4Locked == 0)
        {
            SceneManager.LoadScene("Level4", LoadSceneMode.Single);
        }
    }

    public void LoadLevel5()
    {
        if(SaveManager.instance.playerData.level5Locked == 0)
        {
            SceneManager.LoadScene("Level5", LoadSceneMode.Single);
        }
    }
}
