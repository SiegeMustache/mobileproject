﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public Action ResetLevel;
    public Action ExtraResetLevel;
    public Action PauseLevel;
    public Action ResumeLevel;
    public Action PlayerWin;
    public Action PlayerLose;
    
    [HideInInspector]
    public Scene m_Scene;

    private void Update()
    {
        m_Scene = SceneManager.GetActiveScene();
    }

    private void OnApplicationQuit()
    {
        Destroy(this.gameObject);
    }

    public void ResetActiveLevel()
    {
        if(ResetLevel != null)
        {
            ResetLevel();
        }
    }

    public void ExtraResetActiveLevel()
    {
        if(ExtraResetLevel != null)
        {
            ExtraResetLevel();
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        if(PauseLevel != null)
        {
            PauseLevel();
        }
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        if(ResumeLevel != null)
        {
            ResumeLevel();
        }
    }

    public void PlayerWins()
    {
        if(PlayerWin != null)
        {
            PlayerWin();
        }
        TimerManager.instance.SaveTimer(TimerManager.instance.timer);
    }

    public void PlayerLost()
    {
        if(PlayerLose != null)
        {
            PlayerLose();
        }
        AudioManager.instance.PlayClip(AudioManager.instance.audioClips[0], 0.6f);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void LoadNextLevel()
    {
        if(m_Scene.name == "Level1")
        {
            SceneManager.LoadScene("Level2", LoadSceneMode.Single);
        }
        else if(m_Scene.name == "Level2")
        {
            SceneManager.LoadScene("Level3", LoadSceneMode.Single);
        }
        else if(m_Scene.name == "Level3")
        {
            SceneManager.LoadScene("Level4", LoadSceneMode.Single);
        }
        else if(m_Scene.name == "Level4")
        {
            SceneManager.LoadScene("Level5", LoadSceneMode.Single);
        }
    }
}
