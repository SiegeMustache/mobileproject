﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : Singleton<SaveManager>
{
    [HideInInspector]
    public SaveData playerData;
    [HideInInspector]
    public int currentLevel;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Delete))
        {
            ResetData();
        }

        if(GameManager.instance.m_Scene.name == "Level1")
        {
            currentLevel = 1;
        }
        else if (GameManager.instance.m_Scene.name == "Level2")
        {
            currentLevel = 2;
        }
        else if (GameManager.instance.m_Scene.name == "Level3")
        {
            currentLevel = 3;
        }
        else if (GameManager.instance.m_Scene.name == "Level4")
        {
            currentLevel = 4;
        }
        else if (GameManager.instance.m_Scene.name == "Level5")
        {
            currentLevel = 5;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        playerData = new SaveData();
        if(PlayerPrefs.HasKey("Level1Locked") == false)
        {
            ResetData();
        }
        else if(PlayerPrefs.HasKey("Level1Locked") == true)
        {
            LoadData();
        }
    }

    private void OnApplicationQuit()
    {
        Destroy(this.gameObject);
    }

    public void ResetData()
    {
        playerData.level1Locked = 0;
        playerData.level1Time = 0;
        playerData.level2Locked = 1;
        playerData.level2Time = 0;
        playerData.level3Locked = 1;
        playerData.level3Time = 0;
        playerData.level4Locked = 1;
        playerData.level4Time = 0;
        playerData.level5Locked = 1;
        playerData.level5Time = 0;

        SaveData();
    }

    public void SaveData()
    {
        PlayerPrefs.SetFloat("Level1Timer", playerData.level1Time);
        PlayerPrefs.SetFloat("Level2Timer", playerData.level2Time);
        PlayerPrefs.SetFloat("Level3Timer", playerData.level3Time);
        PlayerPrefs.SetFloat("Level4Timer", playerData.level4Time);
        PlayerPrefs.SetFloat("Level5Timer", playerData.level5Time);

        PlayerPrefs.SetInt("Level1Locked", playerData.level1Locked);
        PlayerPrefs.SetInt("Level2Locked", playerData.level2Locked);
        PlayerPrefs.SetInt("Level3Locked", playerData.level3Locked);
        PlayerPrefs.SetInt("Level4Locked", playerData.level4Locked);
        PlayerPrefs.SetInt("Level5Locked", playerData.level5Locked);
    }

    public void LoadData()
    {
        if (PlayerPrefs.HasKey("Level1Timer"))
        {
            playerData.level1Time = PlayerPrefs.GetFloat("Level1Timer");
        }
        if (PlayerPrefs.HasKey("Level2Timer"))
        {
            playerData.level2Time = PlayerPrefs.GetFloat("Level2Timer");
        }
        if (PlayerPrefs.HasKey("Level3Timer"))
        {
            playerData.level3Time = PlayerPrefs.GetFloat("Level3Timer");
        }
        if (PlayerPrefs.HasKey("Level4Timer"))
        {
            playerData.level4Time = PlayerPrefs.GetFloat("Level4Timer");
        }
        if (PlayerPrefs.HasKey("Level5Timer"))
        {
            playerData.level5Time = PlayerPrefs.GetFloat("Level5Timer");
        }

        if (PlayerPrefs.HasKey("Level1Locked"))
        {
            playerData.level1Locked = PlayerPrefs.GetInt("Level1Locked");
        }
        if (PlayerPrefs.HasKey("Level2Locked"))
        {
            playerData.level2Locked = PlayerPrefs.GetInt("Level2Locked");
        }
        if (PlayerPrefs.HasKey("Level3Locked"))
        {
            playerData.level3Locked = PlayerPrefs.GetInt("Level3Locked");
        }
        if (PlayerPrefs.HasKey("Level4Locked"))
        {
            playerData.level4Locked = PlayerPrefs.GetInt("Level4Locked");
        }
        if (PlayerPrefs.HasKey("Level5Locked"))
        {
            playerData.level5Locked = PlayerPrefs.GetInt("Level5Locked");
        }
    }
}
