﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{

    public int level1Locked;
    public int level2Locked;
    public int level3Locked;
    public int level4Locked;
    public int level5Locked;

    public float level1Time;
    public float level2Time;
    public float level3Time;
    public float level4Time;
    public float level5Time;

    public SaveData()
    {
        level1Locked = 0;
        level2Locked = 1;
        level3Locked = 1;
        level4Locked = 1;
        level5Locked = 1;

        level1Time = 0;
        level2Time = 0;
        level3Time = 0;
        level4Time = 0;
        level5Time = 0;
    }

    public (float, float) ConvertIntoTime(float timer)
    {
        float minutes = Mathf.Floor(timer / 60);
        float seconds = Mathf.FloorToInt(timer % 60);

        return (minutes, seconds);
    }

}
