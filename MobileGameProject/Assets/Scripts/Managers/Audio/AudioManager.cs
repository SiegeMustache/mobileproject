﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : Singleton<AudioManager>
{
    public AudioClip[] audioClips;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlayClip(AudioClip clip, float volume)
    {
        GameObject pulledSource = PoolManager.instance.GetObjectFromPool("Audio");

        pulledSource.transform.position = transform.position;
        AudioSource mySource = pulledSource.GetComponent<AudioSource>();

        mySource.clip = clip;
        mySource.volume = volume;

        pulledSource.SetActive(true);
    }

    public void PlayClip(AudioClip clip, float volume, bool loop)
    {
        GameObject pulledSource = PoolManager.instance.GetObjectFromPool("Audio");

        pulledSource.transform.position = transform.position;
        AudioSource mySource = pulledSource.GetComponent<AudioSource>();

        mySource.clip = clip;
        mySource.loop = loop;
        mySource.volume = volume;

        pulledSource.SetActive(true);
    }
}
