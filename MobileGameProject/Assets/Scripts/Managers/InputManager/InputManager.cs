﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputManager : Singleton<InputManager>
{
    public LayerMask buttonLayer;
    public Canvas inGameUI;

    [HideInInspector]
    public Touch[] touch;

    private Vector2 startPosition;
    private Vector2 endPosition;
    private float startTime;
    

    private void Start()
    {
        ResetTouchInput();
        touch = new Touch[2];
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount == 1)
        {
            touch[0] = Input.GetTouch(0);
            switch(touch[0].phase)
            {
                case TouchPhase.Began:
                    {
                        startPosition = touch[0].position;
                        startTime = Time.time;
                        Tap(touch[0]);
                        break;
                    }
                case TouchPhase.Canceled:
                    {
                        ResetTouchInput();
                        break;
                    }
            }
        }
    }

    private void Tap(Touch touch)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(touch.position);

        GraphicRaycaster m_Raycaster;
        PointerEventData m_PointerEventData;
        EventSystem m_EventSystem;

        //Fetch the Raycaster from the GameObject (the Canvas)
        m_Raycaster = inGameUI.GetComponent<GraphicRaycaster>();
        //Fetch the Event System from the Scene
        m_EventSystem = inGameUI.GetComponent<EventSystem>();

        //Set up the new Pointer Event
        m_PointerEventData = new PointerEventData(m_EventSystem);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = Input.mousePosition;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        m_Raycaster.Raycast(m_PointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results)
        {
            if(result.gameObject.tag == "UI")
            {
                if(result.gameObject.GetComponent<Button>() != null)
                {
                    result.gameObject.GetComponent<Button>().onClick.Invoke();
                    return;
                }
            }
        }

        
        if (PlayerController.instance != null)
        {
            PlayerController.instance.ToggleSnap();
            return;
        }
       
    }

    private void ResetTouchInput()
    {
        startPosition = Vector2.zero;
        endPosition = Vector2.zero;
        startTime = 0;
    }
}
