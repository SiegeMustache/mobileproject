﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TapController : MonoBehaviour
{
    public UnityEvent OnTapTouch;

    public void OnTap()
    {
        OnTapTouch.Invoke();
    }
}
