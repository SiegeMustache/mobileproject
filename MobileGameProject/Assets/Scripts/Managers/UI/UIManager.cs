﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{

    public Text livesText;
    public Text timer;
    public Text winTimer;

    public GameObject hudUI;
    public GameObject winUI;
    public GameObject lostUI;
    public GameObject pauseUI;

    private void Awake()
    {
        GameManager.instance.ResetLevel += ActivateHUDUI;
        GameManager.instance.ExtraResetLevel += ActivateHUDUI;
        GameManager.instance.PlayerWin += ActivateWinUI;
        GameManager.instance.PlayerWin += PrintWinTimer;
        GameManager.instance.PlayerLose += ActivateLostUI;
    }

    private void OnDestroy()
    {
        GameManager.instance.ResetLevel -= ActivateHUDUI;
        GameManager.instance.ExtraResetLevel -= ActivateHUDUI;
        GameManager.instance.PlayerWin -= ActivateWinUI;
        GameManager.instance.PlayerWin -= PrintWinTimer;
        GameManager.instance.PlayerLose -= ActivateLostUI;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        livesText.text = PlayerController.instance.currentLives.ToString();
        timer.text = TimerManager.instance.minutes.ToString() + " : " + TimerManager.instance.seconds.ToString();
    }

    public void ExtraResetGameManager()
    {
        GameManager.instance.ExtraResetActiveLevel();
    }

    public void PauseGame()
    {
        GameManager.instance.PauseGame();
    }

    public void Resumegame()
    {
        GameManager.instance.ResumeGame();
    }

    public void Restart()
    {
        GameManager.instance.ResetActiveLevel();
        Resumegame();
    }

    public void LoadMainMenu()
    {
        GameManager.instance.LoadMainMenu();
    }

    public void LoadNextLevel()
    {
        GameManager.instance.LoadNextLevel();
    }

    public void ActivateWinUI()
    {
        hudUI.SetActive(false);
        winUI.SetActive(true);
        lostUI.SetActive(false);
        pauseUI.SetActive(false);
    }

    public void ActivateLostUI()
    {
        hudUI.SetActive(false);
        winUI.SetActive(false);
        lostUI.SetActive(true);
        pauseUI.SetActive(false);
    }

    public void ActivateHUDUI()
    {
        hudUI.SetActive(true);
        winUI.SetActive(false);
        lostUI.SetActive(false);
        pauseUI.SetActive(false);
    }

    public void ShowAd()
    {
        AdsManager.instance.ShowRewardedAd();
    }

    public void PrintWinTimer()
    {
        winTimer.text = TimerManager.instance.minutes.ToString() + " : " + TimerManager.instance.seconds.ToString();
    }
}
