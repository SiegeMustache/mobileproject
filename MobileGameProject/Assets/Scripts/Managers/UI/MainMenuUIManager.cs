﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUIManager : MonoBehaviour
{
    public Text level1Timer;
    public Text level2Timer;
    public Text level3Timer;
    public Text level4Timer;
    public Text level5Timer;

    // Start is called before the first frame update
    void Start()
    {
        if(SaveManager.instance.playerData.level1Time == 0)
        {
            level1Timer.text = "--:--";
        }
        else if(SaveManager.instance.playerData.level1Time != 0)
        {
            level1Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level1Time);
        }

        if (SaveManager.instance.playerData.level2Time == 0)
        {
            level2Timer.text = "--:--";
        }
        else if (SaveManager.instance.playerData.level2Time != 0)
        {
            level2Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level2Time);
        }

        if (SaveManager.instance.playerData.level3Time == 0)
        {
            level3Timer.text = "--:--";
        }
        else if (SaveManager.instance.playerData.level3Time != 0)
        {
            level3Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level3Time);
        }

        if (SaveManager.instance.playerData.level4Time == 0)
        {
            level4Timer.text = "--:--";
        }
        else if (SaveManager.instance.playerData.level4Time != 0)
        {
            level4Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level4Time);
        }

        if (SaveManager.instance.playerData.level5Time == 0)
        {
            level5Timer.text = "--:--";
        }
        else if (SaveManager.instance.playerData.level5Time != 0)
        {
            level5Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level5Time);
        }

        AudioManager.instance.PlayClip(AudioManager.instance.audioClips[1], 0.6f, true);

    }

    // Update is called once per frame
    void Update()
    {
        if (SaveManager.instance.playerData.level1Time == 0)
        {
            level1Timer.text = "--:--";
        }
        else if (SaveManager.instance.playerData.level1Time != 0)
        {
            level1Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level1Time);
        }

        if (SaveManager.instance.playerData.level2Time == 0)
        {
            level2Timer.text = "--:--";
        }
        else if (SaveManager.instance.playerData.level2Time != 0)
        {
            level2Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level2Time);
        }

        if (SaveManager.instance.playerData.level3Time == 0)
        {
            level3Timer.text = "--:--";
        }
        else if (SaveManager.instance.playerData.level3Time != 0)
        {
            level3Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level3Time);
        }

        if (SaveManager.instance.playerData.level4Time == 0)
        {
            level4Timer.text = "--:--";
        }
        else if (SaveManager.instance.playerData.level4Time != 0)
        {
            level4Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level4Time);
        }

        if (SaveManager.instance.playerData.level5Time == 0)
        {
            level5Timer.text = "--:--";
        }
        else if (SaveManager.instance.playerData.level5Time != 0)
        {
            level5Timer.text = ConvertIntoTimer(SaveManager.instance.playerData.level5Time);
        }
    }

    private string ConvertIntoTimer(float timer)
    {
        int seconds = Mathf.FloorToInt(timer % 60);
        int minutes = (int)Mathf.Floor(timer / 60);

        string result = minutes.ToString() + " : " + seconds.ToString();
        return result;
    }

    public void PlayClickSound()
    {
        AudioManager.instance.PlayClip(AudioManager.instance.audioClips[0], 0.9f, false);
    }

    public void ResetData()
    {
        SaveManager.instance.ResetData();
    }
}
